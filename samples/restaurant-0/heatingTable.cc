#include "heatingTable.hh"
#include "personnel.hh"

#include <mutex>
#include <condition_variable>
#include <iostream>
#include <sstream>

using std::mutex;
using std::condition_variable;
using std::cout;
using std::flush;
using std::ostringstream;

typedef std::unique_lock<std::mutex> locker;

namespace {
void out(ostringstream &os) {
    os << "\n";
    cout << os.str() << flush;
    os.str("");
}
}


HeatingTable::HeatingTable(int maxOn, int max)
  : nosOnTable(0)
  , nosProduced(0)
  , TABLE_SIZE(maxOn)
  , NOS_TO_BE_MADE(max)
  , end(false)
{
}

void HeatingTable::putPlate(Chef const &c) {
    ostringstream s;
    locker lock(mtx);

    while (nosOnTable == TABLE_SIZE) {
	out((s << "Chef " << c.id() << " WAITING; on heating table " << nosOnTable, s));
	notFull.wait(lock);
    }

    nosOnTable++;
    out((s << "Chef " << c.id() << " puts dish on slot " << nosOnTable, s));
    notEmpty.notify_one();
}

bool HeatingTable::getPlate(Waiter const &w) {
    ostringstream s;
    locker lock(mtx);

    while (nosOnTable == 0 && !end) {
	out((s << "Waiter " << w.id() << " WAITING; on heating table " << nosOnTable, s));
	notEmpty.wait(lock);
    }

    if (nosOnTable > 0) {
	nosOnTable--;
	out((s << "Waiter number " << w.id() << " serves a dish", s));
	notFull.notify_one();
	return true;
    } else {
	return false;
    }
}

int HeatingTable::getOrder() {
    locker lock(mtx);

    if (nosProduced < NOS_TO_BE_MADE) {
	++nosProduced;
	return nosProduced;
    } else {
	return 0;
    }
}

void HeatingTable::stop() {
    locker lock(mtx);

    notEmpty.notify_all();
    end = true;
}
