#include <mutex>
#include <condition_variable>

class Chef;
class Waiter;

class HeatingTable {
public:
    HeatingTable(int maxOn, int max);
    void putPlate(Chef const &c);
    bool getPlate(Waiter const &w);
    int getOrder();
    void stop();

private:
    int nosOnTable;
    int nosProduced;
    const int TABLE_SIZE ;
    const int NOS_TO_BE_MADE;
    std::mutex mtx;
    std::condition_variable notFull;
    std::condition_variable notEmpty;
    bool end;
};
