#include "personnel.hh"
#include "heatingTable.hh"

#include <chrono>
#include <thread>
#include <iostream>
#include <random>
#include <sstream>
#include <mutex>

using std::chrono::milliseconds;
using std::this_thread::sleep_for;
using std::cout;
using std::flush;
using std::cerr;
using std::mutex;
using std::lock_guard;
using std::ostringstream;
using std::ranlux24_base;
using std::uniform_int_distribution;

namespace {
class SyncRand {
public:
    SyncRand()
      : dist(1, 8)
    {
    }
    uint32_t next() {
	lock_guard<mutex> l(mtx);
	return dist(rng);
    }

private:
    ranlux24_base rng;
    uniform_int_distribution<uint32_t> dist;
    mutex mtx;
};
SyncRand chefsRand;
SyncRand waitersRand;


void out(ostringstream &os) {
    os << "\n";
    cout << os.str() << flush;
    os.str("");
}
}


Chef::Chef(HeatingTable *shared, int index)
  : sharedTable(shared)
  , ind(index)
{
}

void Chef::operator()() {
    ostringstream s;
    int orderNo;
    while ((orderNo = sharedTable->getOrder())) {
	uint32_t sleepTime = 300 * chefsRand.next();
	out((s << "Chef number " << ind << " works for " << sleepTime << "ms on dish " << orderNo, s));
	sleep_for(milliseconds(sleepTime));
	sharedTable->putPlate(*this);
    }

    out((s << "Chef number " << ind << " finished", s));
}

Waiter::Waiter(HeatingTable *shared, int index)
  : sharedTable(shared)
  , ind(index)
{
}

void Waiter::operator()() {
    ostringstream s;
    while ((sharedTable->getPlate(*this))) {
	uint32_t sleepTime = 200 * waitersRand.next();
	out((s << "Waiter number " << ind << " works for " << sleepTime << "ms", s));
	sleep_for(milliseconds(sleepTime));
    }

    out((s << "Waiter number " << ind << " finished", s));
}
