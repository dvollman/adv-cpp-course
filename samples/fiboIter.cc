#include <iterator>
#include <list>
#include <vector>
#include <iostream>
#include <algorithm>


class FiboIter : public std::iterator <std::input_iterator_tag, int>
{
public:
    FiboIter() : f0(0), f1(1), count(0) {}
    explicit FiboIter(int n) : f0(0), f1(0), count(n) {}
    int operator*() const { return f1; }
    int const *operator->() const { return &f1; }
    const FiboIter &operator++()
    {
        int tmp = f1; f1 = f0 + f1; f0 = tmp;
        ++count;
        return *this;
    }
    FiboIter operator++(int)
    {
        dummy d(this);
        return *this;
    }
    bool operator==(const FiboIter &rhs) const
    { return count == rhs.count; }
    bool operator!=(const FiboIter &rhs)const
    { return count != rhs.count; }

private:
    int f0, f1;
    int count;
    class dummy
    {
    public:
        dummy(FiboIter *i) : iter(i) {}
        ~dummy() { ++(*iter); }
    private:
        FiboIter *iter;
    };
};

int main()
{
    std::list<int> fib(FiboIter(), FiboIter(5));
    std::copy(FiboIter(), FiboIter(10),
              std::back_inserter(fib));
    std::copy(fib.begin(), fib.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << '\n';

    std::vector<int> fib2(20);
    fib2.clear();
    std::copy(FiboIter(), FiboIter(20),
              std::back_inserter(fib2));
    std::copy(fib2.begin(), fib2.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << '\n';


    return 0;
}
