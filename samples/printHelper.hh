// simple helper to print containers

#include <iostream>

template <typename C>
void printOut(C const &cont)
{
    for (typename C::value_type i: cont)
    {
        std::cout << i << ' ';
    }
    std::cout << '\n';
}

template <typename C, typename F>
void printOut(C const &cont, F extract)
{
    for (typename C::value_type const &i: cont)
    {
        std::cout << extract(i) << ' ';
    }
    std::cout << '\n';
}
