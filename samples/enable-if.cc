// example from sfinae.cc simpler with enable_if

#include <iostream>
#include <type_traits>
#include <functional>

using std::cout;

#define VER2 1

void f1(int i)
{
    cout << "f(" << i << ")\n";
}
void f2(int *p)
{
    cout << "f(" << p << ")\n";
}

// goal: a single function that calls f1() for int and f2() for int*

#if defined(VER1)
template <typename T>
typename std::enable_if_t<std::is_integral_v<T>> f(T val)
{
    f1(val);
}

template <typename T>
typename std::enable_if_t<std::is_pointer_v<T>> f(T val)
{
    f2(val);
}

#elif defined(VER2)
template <typename T>
typename std::enable_if_t<std::is_invocable_v<decltype(f1), T>> f(T val)
{
    // see also <https://stackoverflow.com/questions/48167204>
    //f1(val); // not a good idea  // @@@ find example where it doesn't work
    // from p1186r0.html:
    // Otherwise, types that meet the constraint but aren't usable with normal function call syntax (i.e. pointers to member functions and pointers to member data) will trigger hard errors.
    std::invoke(f1, val);
}

template <typename T>
typename std::enable_if_t<std::is_invocable_v<decltype(f2), T>> f(T val)
{
    //f2(val); // not a good idea
    std::invoke(f2, val);
}
#endif


int main()
{
    int i = 5;
    f(i);
    f(&i);

    return 0;
}
