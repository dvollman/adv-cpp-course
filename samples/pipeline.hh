#pragma once
#ifndef PIPELINE_HH_INCLUDES
#define PIPELINE_HH_INCLUDES

#include "queue.hh"

#include <experimental/execution>

#include <tuple>
#include <type_traits>
#include <functional>
#include <utility>

#include <iostream>
//#define LOG (std::cerr << __LINE__ << '\n')

namespace own
{
using std::tuple;
using std::make_tuple;
using std::move;

namespace execution = std::experimental::execution;

constexpr size_t qSize = 3;

enum class ReturnState
{
    none,
    finished,
    valid,
};

// helper functions

// dummy for error messages
void dummy(int);
void dummy(int, int, int, int);

template<class Executor, class Function>
std::enable_if_t<std::is_function_v<Function> || (std::is_pointer_v<Function> && std::is_function_v<std::remove_pointer_t<Function>>), Executor>
require(Executor ex, Function const &)
{
    return ex;
}

template <int N, class List, typename std::enable_if<N==0, int>::type = 0>
auto appendQList(List &lst)
{
    return std::tuple<>{};
}

template <int N, class List, typename std::enable_if<(N>0), int>::type = 0>
auto appendQList(List &lst)
{
    auto &elem = std::get<std::tuple_size<List>::value - N>(lst);
    typedef decltype(elem) TRef;
    typedef typename std::remove_reference_t<TRef>::outT T;
    return tuple_cat(make_tuple(QHandle<T>(qSize)), appendQList<N-1>(lst));
}

template <class FrontTaskT, class MiddleListT>
auto makeQueueList(FrontTaskT &front, MiddleListT &middle)
{
    //auto init = make_tuple(buffer_queue<typename FrontTaskT::outT>(qSize));

    //return std::apply(appendQueue, tuple_cat(init, middle));
    return tuple_cat(make_tuple(QHandle<typename FrontTaskT::outT>(qSize)),
                     appendQList<std::tuple_size<MiddleListT>::value>(middle));
}

template <int N, class Exec, class QListT, class TListT,
          typename std::enable_if<N==0, int>::type = 0>
void startMTask(Exec, QListT &, TListT &)
{
}

template <int N, class Exec, class QListT, class TListT,
          typename std::enable_if<(N>0), int>::type = 0>
void startMTask(Exec ex, QListT &qList, TListT &tList)
{
    auto &task = std::get<std::tuple_size<TListT>::value - N>(tList);
    // size of qList should be one greater than size of tList
    auto &q1 = std::get<std::tuple_size<QListT>::value - (N+1)>(qList);
    auto &q2 = std::get<std::tuple_size<QListT>::value - N>(qList);
    typedef typename std::remove_reference<decltype(q1)>::type Q1T;
    typedef typename Q1T::value_type Q1V;
    typedef typename std::decay<typename std::decay<decltype(task)>::type::funT>::type TaskT;
    using execution::require;
    // declval can't be used here: it's not used, but "evaluated"
    //auto taskEx = require(ex, std::declval<TaskT>());
    auto taskEx = require(ex, *(static_cast<TaskT *>(nullptr)));
    taskEx.execute([&q1, &q2, &task]
                   {
                       Q1V v;
                       while (q1.wait_pop(v) != queue_op_status::closed)
                       {
                           auto ret = task.runOne(move(v));
                           if (ret.state() == ReturnState::valid)
                           {
                               q2.wait_push(*ret);
                           }
                       }
                       q2.close();
                   });
    startMTask<N-1>(ex, qList, tList);
}



// helper classes
template <typename T>
class ValueWState
{
public:
    typedef T valueType;

    ValueWState(ValueWState &&s)
      : st{s.st}
    {
        if (st == ReturnState::valid)
        {
            new (&val) T(move(s.val));
        }
    }
    ValueWState(ReturnState s) : st{s} {}
    ValueWState(T const &v)
      : st{ReturnState::valid}
    {
        new (&val) T(move(v));
    }

    ~ValueWState()
    {
        if (st == ReturnState::valid)
        {
            void *p = &val;
            static_cast<T*>(p)->~T();
        }
    }

    T &operator*()
    {
        void *p = &val;
        return *static_cast<T*>(p);
    }

    ReturnState state() { return st; }

private:
    ReturnState st;
    typename std::aligned_storage<sizeof(T)>::type val;
};

template <class Ret, class F>
class FrontTask
{
public:
    typedef typename Ret::valueType outT;

    FrontTask(F fun) : f{fun} {}

    Ret runOne()
    {
        return f();
    }

private:
    F f;
};

template <class Ret, class F>
class MiddleTask
{
public:
    //typedef Arg inT;
    typedef typename Ret::valueType outT;
    typedef F funT;

    MiddleTask(F fun) : f(fun) {}

    template <typename Arg>
    Ret runOne(Arg &&a) const
    {
        return f(move(a));
    }

private:
    F f;
};

template <typename Arg>
class BackTask
{
public:
    typedef Arg inT;

    BackTask(void end(Arg)) : f(end) {}

    void runOne(Arg &&a) const
    {
        f(move(a));
    }

private:
    void (*f)(Arg);
};

template <class FrontTaskT, class MiddleListT, class BackTaskT>
class Pipeline
{
public:
    Pipeline(FrontTaskT &&f, MiddleListT &&ml, BackTaskT &&b)
      : front{move(f)}
      , middle{move(ml)}
      , back{move(b)}
    {}

    template <class Exec>
    void run(Exec ex)
    {
        auto qList = makeQueueList(front, middle);
        //typedef std::remove_reference(decltype(qList)) QTT;
        typedef decltype(qList) QTT;
        // check correct queue and task lists
        //dummy(qList, front, middle, back);

        auto f = startBackTask(ex, std::get<std::tuple_size<QTT>::value - 1>(qList));
        startMiddleTasks(ex, qList);
        startFrontTask(ex, std::get<0>(qList));
        
        // just wait for the back task to finish
        f.wait();
    }

private:
    template <class Exec, class QueueT>
    auto startBackTask(Exec ex, QueueT &q) const
    {
        // we could also use a latch, but a future from two-way is simpler
        return execution::require(ex, execution::twoway).twoway_execute(
            [&q, this]
            {
                typename BackTaskT::inT v;
                while (q.wait_pop(v) != queue_op_status::closed)
                {
                    back.runOne(move(v));
                }
            });
    }

    // we don't need handles to the middle or front tasks
    template <class Exec, class QListT>
    void startMiddleTasks(Exec ex, QListT &qList) const
    {
        startMTask<std::tuple_size<MiddleListT>::value>(ex, qList, middle);
    }

    template <class Exec, class QueueT>
    void startFrontTask(Exec ex, QueueT &q)
    {
        ex.execute(
            [&q, this]
            {
                while (true)
                {
                    auto ret = front.runOne();
                    if (ret.state() == ReturnState::finished)
                    {
                        break;
                    }
                    if (ret.state() == ReturnState::valid)
                    {
                        q.wait_push(*ret);
                    }
                }
                q.close();
            });
    }

    FrontTaskT front;
    MiddleListT middle;
    BackTaskT back;
};

template <class FrontTaskT, class MiddleListT>
auto makeFrontPipe(FrontTaskT &&f, MiddleListT &&ml);

template <class FrontTaskT, class MiddleListT, typename OutT>
class FrontPipe
{
public:
    typedef OutT outT;

    FrontPipe(FrontTaskT &&f, MiddleListT &&ml)
      : front{move(f)}
      , middle{move(ml)}
    {}

    template <class MiddleTaskT>
    auto cat(MiddleTaskT &&m)
    {
        return makeFrontPipe<typename MiddleTaskT::outT>(
            move(front),
            tuple_cat(move(middle), make_tuple(move(m))));
    }

    template <class BackTaskT>
    auto complete(BackTaskT &&b)
    {
        return Pipeline<FrontTaskT, MiddleListT, BackTaskT>
            (move(front), move(middle), move(b));
    }

private:
    FrontTaskT front;
    MiddleListT middle;
};

template <typename OutT, class FrontTaskT, class MiddleListT>
auto makeFrontPipe(FrontTaskT &&f, MiddleListT &&ml)
{
    return FrontPipe<FrontTaskT, MiddleListT, OutT>(move(f), move(ml));
}

template <class F>
auto makeFrontPipe(F &&f)
{
    std::remove_reference_t<F> *p = nullptr;
    typedef decltype((*p)()) Ret;

    FrontTask<Ret, F> t{move(f)};
    return makeFrontPipe<typename Ret::valueType>(move(t), make_tuple());
}

template <class FrontPipeT, class F>
auto operator|(FrontPipeT &&front, F &&next)
{
    std::remove_reference_t<F> *pfoo = nullptr;
    typename FrontPipeT::outT *parg = nullptr;
    typedef decltype((*pfoo)(*parg)) Ret;
    MiddleTask<Ret, F> t{next};
    return front.cat(move(t));
}

template <class FrontPipeT>
auto operator|(FrontPipeT &&f,
               void add(typename FrontPipeT::outT))
{
    BackTask<typename FrontPipeT::outT> t{add};
    return f.complete(move(t));
}


} // namespace own
#undef LOG
#endif /* PIPELINE_HH_INCLUDES */
