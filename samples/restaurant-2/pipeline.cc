#include <pipeline.h>
#include <simple_thread_pool.h>
#include <iostream>
#include <unistd.h>
#include <chrono>
#include <thread>
#include <random>
#include <sstream>
#include <mutex>

using std::chrono::milliseconds;
using std::this_thread::sleep_for;
using std::cout;
using std::flush;
using std::cerr;
//using std::mutex;
//using std::lock_guard;
using std::ostringstream;
using std::ranlux24_base;
using std::uniform_int_distribution;



namespace {
class SyncRand {
public:
    SyncRand()
      : dist(1, 8)
    {
    }
    uint32_t next() {
        std::lock_guard<std::mutex> l(mtx);
        return dist(rng);
    }

private:
    ranlux24_base rng;
    uniform_int_distribution<uint32_t> dist;
    std::mutex mtx;
};
SyncRand chefsRand;
SyncRand waitersRand;


void out(ostringstream &os) {
    os << "\n";
    cout << os.str() << flush;
    os.str("");
}
}


void dispenseOrder(gcl::queue_back<int> out)
{
    for (int i = 0; i < 20; ++i)
    {
        out.push(i);
    }
}

int cook(int order) {
    ostringstream s;

    uint32_t sleepTime = 300 * chefsRand.next();
    out((s << "Chef works for " << sleepTime << "ms on dish " << order, s));
    sleep_for(milliseconds(sleepTime));
    return order;
}

void serve(int order) {
    ostringstream s;

    uint32_t sleepTime = 200 * waitersRand.next();
    out((s << "Waiter serves for " << sleepTime << "ms dish " << order, s));
    sleep_for(milliseconds(sleepTime));
}

int main()
{
    auto orders = gcl::pipeline::from(dispenseOrder);
    auto chef = gcl::pipeline::make(cook);
    auto waiter = gcl::pipeline::to(serve);

    gcl::pipeline::plan restaurant(
        orders |
        gcl::pipeline::parallel(chef, 3) |
        gcl::pipeline::parallel(waiter, 4));
    gcl::simple_thread_pool pool;

    gcl::pipeline::execution work(restaurant.run(&pool));
    work.wait();
    cout << std::endl;
}
