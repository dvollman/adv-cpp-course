#ifndef PERSONNEL_HH_SEEN
#define PERSONNEL_HH_SEEN
#include "restaurant.hh"

#include <buffer_queue.h>
#include <latch.h>

class Chef {
public:
    Chef(QueueType *shared, OrderDispenser *,
         gcl::latch *finish, int ind);
    void operator()();
    int id() const { return ind; }

private:
    QueueType *sharedTable;
    OrderDispenser *orders;
    gcl::latch *stopLatch;
    int ind;
};

class Waiter {
public:
    Waiter(QueueType *shared, int ind);
    void operator()();
    int id() const { return ind; }

private:
    QueueType *sharedTable;
    int ind;
};
#endif /* PERSONNEL_HH_SEEN */
