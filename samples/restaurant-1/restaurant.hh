#ifndef RESTAURANT_HH_SEEN
#define RESTAURANT_HH_SEEN

#include <buffer_queue.h>
#include <mutex>

typedef gcl::buffer_queue<int> QueueType;

class OrderDispenser {
public:
    explicit OrderDispenser(int maxOrders)
      : max(maxOrders)
      , val(0)
    {
    }

    int nextOrder();

private:
    typedef std::lock_guard<std::mutex> Locker;
    std::mutex mtx;

    int max;
    int val;
};
#endif /* RESTAURANT_HH_SEEN */
