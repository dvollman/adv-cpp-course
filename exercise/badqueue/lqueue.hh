// Locked queue implementation
// doesn't work correctly, DON'T USE!!!
/*
 * Copyright (c) 2004-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#ifndef CPP_MT_LQUEUE_HH
#define CPP_MT_LQUEUE_HH

#include <thread>
#include <mutex>
#include <condition_variable>
#include <deque>


template <typename T>
class LQueue
{
public:
    LQueue() = default;

    void push(T const &item);
    T pop();

    T front() const;
    T back() const;
    unsigned int empty() const;
    unsigned int size() const;
    void erase(void);
    void wait() const;
    void notify();

private:
    std::deque<T> store;
    mutable std::mutex mtx;
    mutable std::condition_variable notEmpty;

};


// implementation
template <typename T>
inline void LQueue<T>::push(T const &item)
{
    std::lock_guard l(mtx);
    store.push_back(item);
}
template <typename T>
inline T LQueue<T>::pop()
{
    std::lock_guard l(mtx);
    T tmp(store.front());
    store.pop_front();
    return tmp;
}

template <typename T>
inline T LQueue<T>::front() const
{
    return store.front();
}
template <typename T>
inline T LQueue<T>::back() const
{
    return store.back();
}
template <typename T>
inline unsigned int LQueue<T>::empty() const
{
    return store.empty();
}
template <typename T>
inline unsigned int LQueue<T>::size() const
{
    return store.size();
}
template <typename T>
inline void LQueue<T>::erase(void)
{
    std::lock_guard l(mtx);
    store.clear();
}
template <typename T>
inline void LQueue<T>::wait() const
{
    std::unique_lock l(mtx);
    notEmpty.wait(l);
}
template <typename T>
inline void LQueue<T>::notify()
{
    notEmpty.notify_one();
}
#endif // CPP_MT_LQUEUE_HH
