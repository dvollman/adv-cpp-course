// implementation for declarations from
#include "graphmock.hh"
#include "allocmock.hh"
#include "test-classes.hh"
#include "cairomock.hh"
#include "xwin-mock.hh"
#include "xmock.hh"

/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <new>
#include <iostream>
#include <cassert>

#if 0
// hardcore debugging
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <charconv>

namespace
{
void writeLog(int line)
{
    static char buffer[10];
    int dbgFd = open("dbg.log", O_WRONLY | O_APPEND | O_CREAT, 00777);
    auto end = std::to_chars(buffer, buffer+10, line).ptr;
    *end = '\n';
    write(dbgFd, buffer, (end-buffer)+1);
    close(dbgFd);
}
} // unnamed namespace
#endif

namespace
{
cairo_t dummyCairo;
cairo_surface_t dummySurface;

exerciseTest::XWinWrap dummyWinObj1(0, 0, std::string());
exerciseTest::XWinWrap dummyWinObj2(0, 0, std::string());
} // unnamed namespace

// graphmock.hh
namespace exerciseTest
{
XWinWrap *dummyWin1 = &dummyWinObj1;
XWinWrap *dummyWin2 = &dummyWinObj2;
} // namespace exerciseTest

inline namespace graphMock
{
using namespace exerciseTest;

Display *XOpenDisplay(char const *name)
{
    return XWrap::getCurrent().XOpenDisplay(name);
}
int XCloseDisplay(Display *dpy)
{
    return XWrap::getCurrent().XCloseDisplay(dpy);
}

Window XCreateSimpleWindow(
    Display *dpy
    , Window rootW
    , int x, int y
    , unsigned int w, unsigned int h
    , unsigned int bw
    , unsigned long bcol, unsigned long bgcol)
{
    return XWrap::getCurrent().XCreateSimpleWindow(dpy, rootW, x, y, w, h, bw, bcol, bgcol);
}
int XDestroyWindow(Display *dpy, Window w)
{
    return XWrap::getCurrent().XDestroyWindow(dpy, w);
}

int XNextEvent(Display *dpy, XEvent *ev)
{
    return XWrap::getCurrent().XNextEvent(dpy, ev);
}

int XMaskEvent(Display *, long, XEvent *)
{
    return 1;
}


cairo_surface_t *cairo_xlib_surface_create(
    Display *dpy
    , Drawable d
    , Visual *v
    , int w
    , int h)
{
    assert(1);
    cairo_surface_t *tmp = nullptr;
    if (d)
    {
        tmp = d->myCairo;
    }
    return tmp;
}

void cairo_surface_destroy(cairo_surface_t *s)
{
    if (globalGuiSeq) globalGuiSeq->store(surfaceDestroy);
    if (globalForbidden) globalForbidden->call(surfaceDestroy);
    if (CairoWrap::curMock)
    {
        //std::cerr << __FUNCTION__ << ": obj: " << CairoWrap::curObj << '\n';
        CairoWrap::curMock->cairo_surface_destroy(s);
    }
    if (s) s->CairoWrap::cairo_surface_destroy(s);
}

cairo_t *cairo_create(cairo_surface_t *s)
{
    if (globalGuiSeq) globalGuiSeq->store(cairoCreate);
    if (globalForbidden) globalForbidden->call(cairoCreate);
    if (CairoWrap::curMock)
    {
        //std::cerr << __FUNCTION__ << ": obj: " << CairoWrap::curObj << '\n';
        CairoWrap::curMock->cairo_create(s);
    }
    if (s) return s->CairoWrap::cairo_create(s);
    return &dummyCairo;
}

void cairo_destroy(cairo_t *cr)
{
    if (globalGuiSeq) globalGuiSeq->store(cairoDestroy);
    if (globalForbidden) globalForbidden->call(cairoDestroy);
    if (CairoWrap::curMock)
    {
        //std::cerr << __FUNCTION__ << ": obj: " << CairoWrap::curObj << '\n';
        CairoWrap::curMock->cairo_destroy(cr);
    }
    if (cr) cr->CairoWrap::cairo_destroy(cr);
}

void cairo_show_text(cairo_t *cr, char const *txt)
{
    //CairoWrap::getCurrent().cairo_show_text(cr, txt);
}
} // namespace graphMock


namespace exerciseTest
{
// allocmock.hh
FixedVect<AllocMock *, 10> AllocMock::allocs;

// we need always at least one alloc
//AllocMock globalTopAlloc;


// test-classes.hh
Sequencer<GuiFuncs> *globalGuiSeq = nullptr;


void Forbidden::print(std::ostream &o) const
{
    o << "Failed: forbidden call of " << guiFuncsNames[funcName] << '\n';
}

ForbiddenCall::ForbiddenCall(std::initializer_list<GuiFuncs> const &l)
  : forbiddenFuncs(l.begin(), l.end())
{
}

void ForbiddenCall::call(GuiFuncs what)
{
    if (forbiddenFuncs.find(what) != forbiddenFuncs.end())
    {
        forbiddenFunctionCalled = true;
        throw Forbidden(what);
    }
}

ForbiddenCall *globalForbidden = nullptr;


TestShape::TestShape(int i)
  : id(i)
{}

TestShape::TestShape(int i, Sequencer<int> *seq)
  : id{i}
  , drawIdMarker{seq}
{}

TestShape::TestShape(TestShape &&other)
  : Shape{std::move(other)}
  , id{other.id}
  , destructionMarker{other.destructionMarker}
  , drawIdMarker{other.drawIdMarker}
  , drawFuncMarker{other.drawFuncMarker}
{
    other.id = 0;
    other.destructionMarker = nullptr;
    other.drawIdMarker = nullptr;
    other.drawFuncMarker = nullptr;
}

TestShape::~TestShape()
{
    if (destructionMarker)
    {
        *destructionMarker = true;
    }
    if (globalGuiSeq)
    {
        globalGuiSeq->store(shapeClean);
    }
}

TestShape &TestShape::operator=(TestShape &&rhs)
{
    Shape::operator=(std::move(rhs));
    id = rhs.id;
    destructionMarker = rhs.destructionMarker;
    drawIdMarker = rhs.drawIdMarker;

    rhs.destructionMarker = nullptr;
    rhs.drawIdMarker = nullptr;

    return *this;
}

void TestShape::doDraw(cairo_t *) const
{
    if (drawIdMarker) drawIdMarker->store(id);
    if (drawFuncMarker) drawFuncMarker->store(shapeDraw);
}

namespace detail
{
class GraphErrorReporter
{
public:
    GraphErrorReporter() = default;

    void setError(char const *file, unsigned line, std::string const &msg)
    {
        // we only report the first error
        if (err) return;
        srcFile = file;
        srcLine = line;
        errMsg = msg;
        err = true;
    }

    bool ok()
    {
        if (err)
        {
            std::clog << "Problem at " << srcFile << ':' << srcLine << '\n';
            std::clog << errMsg;
            err = false;
            return false;
        }
        return true;
    }

private:
    char const *srcFile = nullptr;
    unsigned srcLine = 0;
    std::string errMsg;
    bool err = false;
};
GraphErrorReporter errReporter;

} // namespace detail

bool testOk()
{
    return detail::errReporter.ok();
}


void setTromploeilReporter()
{
    trompeloeil::set_reporter(
        [] (trompeloeil::severity s, char const *file, unsigned line, std::string const &msg)
        {
            std::cerr << "Problem at " << file << ':' << line << '\n';
            std::cerr << msg;
            if (s == trompeloeil::severity::fatal)
            {
                throw std::runtime_error("Fatal problem");
            } else {
                detail::errReporter.setError(file, line, msg);
            }
        });
}


// cairomock.hh
void CairoWrap::cairo_surface_destroy(cairo_surface_t *)
{
    if (seq) seq->store(surfaceDestroy);
}

cairo_t *CairoWrap::cairo_create(cairo_surface_t *)
{
    if (seq) seq->store(cairoCreate);
    return this;
}

void CairoWrap::cairo_destroy(cairo_t *)
{
    if (seq) seq->store(cairoDestroy);
}

CairoMock *CairoWrap::curMock = nullptr;


// xwin-mock.hh
XWinMock::XWinMock()
{
    oldMock = XWinWrap::mock;
    XWinWrap::mock = this;
}

XWinMock::~XWinMock()
{
    XWinWrap::mock =oldMock;
}

XWinTestHelper::~XWinTestHelper()
{
    if (host)
    {
        host->myHelper = nullptr;
    }
}

void XWinTestHelper::seq(GuiFuncs f)
{
    if (seqObj) seqObj->store(f);
}


XWinWrap::XWinWrap(int, int, std::string const &)
{
    if (globalGuiSeq) globalGuiSeq->store(winCreate);
    if (globalForbidden) globalForbidden->call(winCreate);
    if (helperPtr) myHelper = helperPtr;
    if (myHelper)
    {
        myHelper->seq(winCreate);
        myHelper->host = this;
    }
    if (mock) mock->create(this);
    if (cairoWrapPtr) myCairo = cairoWrapPtr;
    curWin = this;
}

XWinWrap::XWinWrap(XWinWrap &&rhs)
  : myHelper{rhs.myHelper}
  , myCairo{rhs.myCairo}
  , valid{rhs.valid}
{
    rhs.myHelper = nullptr;
    rhs.myCairo = nullptr;
    rhs.valid = false;
}

XWinWrap::~XWinWrap() noexcept(false)
{
    if (globalGuiSeq) globalGuiSeq->store(winClean);
    if (globalForbidden) globalForbidden->call(winClean);
    if (myHelper)
    {
        myHelper->seq(winClean);
        myHelper->host = nullptr;
    }
    if (mock && valid) mock->clean(this);
}

XWinWrap &XWinWrap::operator=(XWinWrap &&rhs)
{
    if (this == &rhs) return *this;

    if (mock && valid) mock->clean(this);
    myHelper = rhs.myHelper;
    rhs.myHelper = nullptr;
    myCairo = rhs.myCairo;
    rhs.myCairo = nullptr;
    valid = rhs.valid;
    rhs.valid = false;

    return *this;
}

cairo_surface_t *XWinWrap::getSurface()
{
    if (myHelper) myHelper->seq(winSurface);
    cairo_surface_t *tmp = myCairo;
    if (!tmp) tmp = &dummySurface;

    if (mock)
    {
        tmp = mock->getSurface();
    }
    return tmp;
}

void XWinWrap::registerCallback(std::function<void()> f)
{
    if (globalGuiSeq) globalGuiSeq->store(registerCb);
    if (globalForbidden) globalForbidden->call(registerCb);
    if (myHelper)
    {
        myHelper->seq(registerCb);
        if (myHelper->cb)
        {
            myHelper->wrongRegisterCbOnFull = true;
        }
        myHelper->cb = f;
    }
    if (mock) mock->registerCb(this, f);
}

void XWinWrap::unregisterCallback()
{
    if (globalGuiSeq) globalGuiSeq->store(unregisterCb);
    if (globalForbidden) globalForbidden->call(unregisterCb);
    if (!getMe())
    {
        std::cerr << "FATAL: unregisterCallback called for nullptr\n";
        detail::errReporter.setError("unknown", 0, "unregisterCallback called for nullptr");
        return;
    }
    if (myHelper)
    {
        myHelper->seq(unregisterCb);
        if (!myHelper->cb)
        {
            myHelper->wrongUnregCbOnEmpty = true;
        }
        myHelper->cb = nullptr;
    }
    if (mock) mock->unregisterCb(this);
}

XWinWrap const *XWinWrap::getMe() const
{
    return this;
}

GuiWinMock *XWinWrap::mock = nullptr;
XWinTestHelper *XWinWrap::helperPtr = nullptr;
CairoWrap *XWinWrap::cairoWrapPtr = nullptr;
XWinWrap *XWinWrap::curWin = nullptr;


// xmock.hh
GuiWrap GuiWrap::defaultObj;
GuiWrap *GuiWrap::curObj = &defaultObj;

} // namespace exerciseTest

void *operator new(size_t n)
{
    auto curAlloc = exerciseTest::AllocMock::getCurrent();
    if (curAlloc)
    {
        return curAlloc->alloc(n);
    } else {
        return malloc(n);
    }
}

void operator delete(void *p) noexcept
{
    auto curAlloc = exerciseTest::AllocMock::getCurrent();
    if (curAlloc)
    {
        return curAlloc->dealloc(p);
    } else {
        return free(p);
    }
}

// for sized deallocation
void operator delete(void *p, long unsigned) noexcept
{
    auto curAlloc = exerciseTest::AllocMock::getCurrent();
    if (curAlloc)
    {
        return curAlloc->dealloc(p);
    } else {
        return free(p);
    }
}
