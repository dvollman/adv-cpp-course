/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#ifndef GRAPHMOCK_HH_SEEN_
#define GRAPHMOCK_HH_SEEN_

#include <array>

namespace exerciseTest
{
struct CairoWrap;
struct XWinWrap;

extern XWinWrap *dummyWin1;
extern XWinWrap *dummyWin2;

enum GuiFuncs
{
    noFunc
    , winCreate
    , winSurface
    , cairoCreate
    , registerCb
    , shapeDraw
    , unregisterCb
    , cairoDestroy
    , surfaceDestroy
    , winClean
    , shapeClean
};

inline constexpr std::array guiFuncsNames =
{
    "noFunc"
    , "winCreate"
    , "winSurface"
    , "cairoCreate"
    , "registerCb"
    , "shapeDraw"
    , "unregisterCb"
    , "cairoDestroy"
    , "surfaceDestroy"
    , "winClean"
    , "shapeClean"
};
} // exerciseTest

inline namespace graphMock
{
// X mocks
typedef int Display;
typedef exerciseTest::XWinWrap *Window;
typedef int Visual;
typedef int Atom;
typedef int Bool;

enum XMaskConstants : long { ExposureMask, ButtonPressMask };
enum XEventType { Expose, ButtonPress, ClientMessage };

struct XEvent
{
    XEventType type;

    struct XExposeEvent
    {
        int count;
        Window window;
    };
    XExposeEvent xexpose;
};
inline bool operator==(XEvent const &a, XEvent const &b)
{
    return a.type == b.type && a.xexpose.window == b.xexpose.window;
}

int XNextEvent(Display *, XEvent *);

Display *XOpenDisplay(char const *name);
int XCloseDisplay(Display *dpy);

Window XCreateSimpleWindow(
    Display *
    , Window
    , int x, int y
    , unsigned int width, unsigned int height
    , unsigned int border_width
    , unsigned long border, unsigned long background);
int XDestroyWindow(Display *, Window);
int XMaskEvent(Display *, long, XEvent *);


inline int DefaultScreen(Display *)
{
    return 0;
}

inline Window RootWindow(Display *, int)
{
    return 0;
}

inline Visual *DefaultVisual(Display *, int)
{
    return nullptr;
}

inline unsigned long BlackPixel(Display *, int)
{
    return 0;
}
inline unsigned long WhitePixel(Display *, int)
{
    return 0;
}

inline int XStoreName(Display *, Window, char const *)
{
    return 0;
}

inline int XSelectInput(Display *, Window, long)
{
    return 0;
}

inline int XSetWMProtocols(Display *, Window, Atom *, int)
{
    return 0;
}

inline Atom XInternAtom(Display *, char const *, Bool)
{
    return 0;
}

inline int XMapWindow(Display *, Window)
{
    return 0;
}

inline int XFlush(Display *)
{
    return 0;
}



// Cairo mocks
typedef exerciseTest::XWinWrap *Drawable;
typedef exerciseTest::CairoWrap cairo_surface_t;
//typedef struct _cairo cairo_t;
typedef exerciseTest::CairoWrap cairo_t;

//struct _cairo
//{
//};

cairo_surface_t *cairo_xlib_surface_create(
    Display *
    , Drawable
    , Visual *
    , int width
    , int height);
void cairo_surface_destroy(cairo_surface_t *);
cairo_t *cairo_create(cairo_surface_t *);
void cairo_destroy(cairo_t *);

inline void cairo_save(cairo_t *)
{
}
inline void cairo_restore(cairo_t *)
{
}

inline void cairo_show_page(cairo_t *)
{
}

inline void cairo_new_sub_path(cairo_t *)
{
}

inline void cairo_set_source_rgb(cairo_t *, double /*red*/, double /*green*/, double /*blue*/)
{
}

inline void cairo_set_line_width(cairo_t *, double /*width*/)
{
}

inline void cairo_arc(
    cairo_t *
    , double /*xc*/, double /*yc*/
    , double /*radius*/
    , double /*angle1*/, double /*angle2*/
    )
{
}

inline void cairo_rectangle(
    cairo_t *
    , double /*x*/, double /*y*/
    , double /*width*/, double /*height*/
    )
{
}

inline void cairo_move_to(
    cairo_t *
    , double /*x*/, double /*y*/
    )
{
}

inline void cairo_fill(cairo_t *)
{
}

inline void cairo_stroke(cairo_t *)
{
}

void cairo_show_text(cairo_t *, char const *);
} // namespace graphMock

// this is not really a graphics mock, but still a mock for graphics testing...
#include "vector-shape.hh"
#endif /* GRAPHMOCK_HH_SEEN_ */
