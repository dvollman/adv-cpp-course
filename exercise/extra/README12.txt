Exercise "Threads w/ async"
base: sieve

This implements the Sieve of Eratosthenes to compute prime numbers.
(The Sieve simply marks all the multiples of (all) numbers in a
prespecified range, and at the end all the unmarked are primes.)
It already partitions the range into slices, but then computes
the slices sequentially.

 - Make the Sieve of Eratosthenes parallel
   - start one async per slice
   - wait for all to finish
   - look at timing statistics vs. single-threaded version
     (possibly move the single slice timer into computeRange)
   - try different numbers of slices
