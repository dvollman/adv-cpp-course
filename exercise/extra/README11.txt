Exercise "Parallelism"
Base: par/count.cc

 - Try to make the counting faster by parallelizing it.
 - Comparing the character w/ mangle2() is deliberately slow
   to make it a computing task and not a memory task.
