#include "func-templ.hh"

#include <iostream>
#include <vector>

//#define HAVE_READ_TEMPL 1
//#define HAVE_PRINT_TEMPL 1
#include "testimpl.hh"

using std::cin;
using std::clog;
using std::cout;
using std::vector;

using namespace exercise;

int main()
{
    exerciseTest::runTests();

    vector<int> vi;

    // read into vector
    bool ok = true;
    while (ok)
    {
        int i;
        cout << "Please enter a number between 1 and 1000, press '.' to stop: ";
        ok = readInt(cin, clog, i, 1, 1000);

        if (ok)
        {
            vi.push_back(i);
        }
    }
    // print
    cout << '\n';
    printVector(cout, vi);

#if defined(HAVE_PRINT_TEMPL) && 0
    cin.clear();
    std::string dummy;
    std::getline(cin, dummy);
    int i = 22;
    cout << "Please enter a number between 0 and 7.5: ";
    readVal(cin, clog, i, 0.0, 7.5f);
    cout << i << '\n';
#endif

    return 0;
}
