#include "lest.hpp"
#include <string>
#include <string_view>
#include <vector>
#include <sstream>
#include <algorithm>

#ifdef HAVE_READ_TEMPL
#define READ readVal
#else
#define READ readInt
#endif

#ifdef HAVE_PRINT_TEMPL
#define PRINT printCont
#else
#define PRINT printVector
#endif

namespace exerciseTest
{
using namespace exercise;
using namespace std::literals;

//std::istringstream is;
//std::ostringstream os;
std::vector lestArgs = {"-p"s, "-a"s};

struct TestX
{
    int i;
};
std::ostream &operator<<(std::ostream &o, TestX t)
{
    o << t.i;
    return o;
}

const lest::test baseTests[] =
{
    CASE("Standard case read")
    {
        int rv;
        std::ostringstream os;
        std::istringstream is("5");
        READ(is, os, rv, 1, 10);

        EXPECT(os.str().size() == 0u);
        EXPECT(rv == 5);
    },
    CASE("Test read with one value below")
    {
        int rv;
        std::ostringstream os;
        std::istringstream is("1 5");
        READ(is, os, rv, 2, 10);

        // string_view doesn't work, as str() returns a copy,
        // not the underlying data
        //std::string_view s = os.str();
        // but RVO let's us take the value directly
        std::string s = os.str();
        EXPECT(std::count(s.begin(), s.end(), '\n') == 1);
        EXPECT(rv == 5);
    },
    CASE("Test read with wrong character")
    {
        int rv = 1;
        std::ostringstream os;
        std::istringstream is("; 5");
        READ(is, os, rv, 2, 10);

        EXPECT(os.str().size() == 0u);
        EXPECT(rv == 1);
    },
    CASE("Test read with one value above")
    {
        int rv;
        std::ostringstream os;
        std::istringstream is("10 5");
        READ(is, os, rv, 2, 8);

        std::string s = os.str();
        EXPECT(std::count(s.begin(), s.end(), '\n') == 1);
        EXPECT(rv == 5);
    },
    CASE("Test read with value on bound")
    {
        int rv;
        std::ostringstream os;
        std::istringstream is("10");
        READ(is, os, rv, 2, 10);

        EXPECT(os.str().size() == 0u);
        EXPECT(rv == 10);
    },
    CASE("Test read with bound1 > bound2")
    {
        int rv;
        std::ostringstream os;
        std::istringstream is("-5");
        READ(is, os, rv, 0, -10);

        EXPECT(os.str().size() == 0u);
        EXPECT(rv == -5);
    },

    CASE("Test simple print")
    {
        std::vector<int> vi = {33, 72, 89, -6};
        std::ostringstream os;

        PRINT(os, vi);

        std::string s = os.str();
        size_t pos = 0;
        size_t newPos = 0;
        newPos = s.find("33", pos);
        EXPECT(newPos != s.npos);
        EXPECT(newPos > pos);
        pos = newPos;

        newPos = s.find("72", pos);
        EXPECT(newPos != s.npos);
        EXPECT(newPos > pos);
        pos = newPos;

        newPos = s.find("89", pos);
        EXPECT(newPos != s.npos);
        EXPECT(newPos > pos);
        pos = newPos;

        newPos = s.find("-6", pos);
        EXPECT(newPos != s.npos);
        EXPECT(newPos > pos);
    },

    CASE("Test empty print")
    {
        std::vector<int> vi;
        std::ostringstream os;

        PRINT(os, vi);

        EXPECT(os.str().size() == 0u);
    },

#ifdef HAVE_READ_TEMPL
    CASE("Template read double")
    {
        double rv;
        std::ostringstream os;
        std::istringstream is("5.0");
        READ(is, os, rv, 1., 10.);

        EXPECT(os.str().size() == 0u);
        EXPECT(rv == 5.0);
    },
    CASE("Template read short")
    {
        short rv;
        std::ostringstream os;
        std::istringstream is("5");
        READ(is, os, rv, short(1), short(10));

        EXPECT(os.str().size() == 0u);
        EXPECT(rv == short(5));
    },
    CASE("Template read string")
    {
        std::string rv;
        std::ostringstream os;
        std::istringstream is("C++");
        READ(is, os, rv, "A", "Z");
        std::clog << os.str();

        EXPECT(os.str().size() == 0u);
        EXPECT(rv == "C++");
    },
#endif

#ifdef HAVE_PRINT_TEMPL
    CASE("Test print string vector")
    {
        std::vector<std::string> vs = {"TeST"s};
        std::ostringstream os;

        PRINT(os, vs);

        std::string s = os.str();
        size_t pos = 0;
        size_t newPos = 0;
        newPos = s.find("TeST", pos);
        EXPECT(newPos != s.npos);
        EXPECT(newPos > pos);
    },

    CASE("Test print vector with TestX")
    {
        std::vector<TestX> vt = {{22}};
        std::ostringstream os;

        PRINT(os, vt);

        std::string s = os.str();
        size_t pos = 0;
        size_t newPos = 0;
        newPos = s.find("22", pos);
        EXPECT(newPos != s.npos);
        EXPECT(newPos > pos);
    },

#endif
};

void runTests()
{
    int status = lest::run(baseTests, lestArgs, std::cout);
    if (status == 0)
    {
        std::cout << "Tests successful\n";
    } else {
        std::cout << "Tests failed\n";
    }
}
} // namespace exerciseTest
