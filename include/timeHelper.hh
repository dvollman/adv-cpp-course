// simple timer for tests
/*
 * Copyright (c) 2014-2019 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#ifndef TIMER_HH_INCLUDED
#define TIMER_HH_INCLUDED
#include <iostream>
#include <chrono>

class Timer
{
public:
    Timer();

    void reset();

    std::ostream &print(std::ostream &o) const;

private:
    static auto now();

    std::chrono::high_resolution_clock::time_point start;
};

std::ostream &operator<<(std::ostream &o, Timer const &t);


// implementation
// first for auto deduction
inline auto Timer::now()
{
    return std::chrono::high_resolution_clock::now();
}

inline Timer::Timer()
  : start(now())
{
}

inline void Timer::reset()
{
    start = now();
}

inline std::ostream &Timer::print(std::ostream &o) const
{
    using namespace std::chrono;
    o << duration_cast<microseconds>(now()-start).count();
    return o;
}

inline std::ostream &operator<<(std::ostream &o, Timer const &t)
{
    return t.print(o);
}
#endif /* TIMER_HH_INCLUDED */
