#pragma once
#ifndef LOG_HELPER_HH_SEEN
#define LOG_HELPER_HH_SEEN
#include <iostream>

#ifndef USE_LOG
#define USE_LOG 0
#endif

#undef LOG
#undef LVAR

#if (USE_LOG)
#define LOG (std::cerr << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << ": log\n")
#define LVAR(x) (std::cerr << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << ": " << #x << ':' << (x) << '\n')
#define LTVAR(msg, x) (std::cerr << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << ": " << msg << ':' << (x) << '\n')
#else
#define LOG ((void)0)
#define LVAR(x) ((void)0)
#define LTVAR(msg, x) ((void)0)
#endif

#endif /* LOG_HELPER_HH_SEEN */
